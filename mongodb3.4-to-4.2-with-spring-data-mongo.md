## 몽고DB 3.4 to 4.2
몽고DB 서버 4.2 - 자바 Driver 버전 호환성  
https://docs.mongodb.com/ecosystem/drivers/driver-compatibility-reference/#java-driver-compatibility 
https://docs.mongodb.com/ecosystem/drivers/reactive-streams/#mongodb-compatibility  
https://mongodb.github.io/mongo-java-driver/4.0/whats-new/   
> 몽고DB 4.2 사용 최소 sync 드라이버 버전은 3.11, reactive 드라이버는 1.12 부터  

몽고DB 서버 4.2 - 자바 Driver 3.11 - Spring Data MongoDB 호환  
https://docs.spring.io/spring-data/data-mongo/docs/2.2.x/changelog.txt  
> 2.2.0.M4 (2019-05-13), 2.2.0.RC1 (2019-06-14) changelog 통해 2.2.0 부터 몽고DB 4.2 지원을 시작하고 있음.  
> 2.2.3.RELEASE (2019-12-04)에서 DATAMONGO-2430 - Upgrade to mongo-java-driver 3.11.2.  

Spring Data MongoDB 2.2.x – Spring Framework 호환  
https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#requirements   
> Spring Data MongoDB 2.x 는 Java 8.0 이상, Spring Framework 5.2.3.RELEASE 이상, 몽고DB 2.6 이상

What’s New in Spring Data MongoDB 2.2  
https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#reference 

spring-data/mongodb Upgrading from 2.x to 3.x     
https://docs.spring.io/spring-data/mongodb/docs/current-SNAPSHOT/reference/html/#upgrading.2-3    


Can we use spring version 4.3 and spring data mongo db version 2.2.3 to connect to mongo db 4.2.  
https://stackoverflow.com/questions/59613288/spring-4-x-and-spring-data-mongo-db-2-x-compatibility  

Compatibility Changes in MongoDB 4.2  
https://docs.mongodb.com/manual/release-notes/4.2-compatibility/  

MongoDB 4.2 vs 4.0, 3.6, 3.4, and 3.2 Benchmarks  
https://medium.com/@hartator/mongodb-4-2-vs-4-0-3-6-3-4-and-3-2-benchmarks-ee96a09ef231  